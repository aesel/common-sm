package systems.aesel.common.sm.util;

import java.io.File;

import systems.aesel.common.sm.Event;

public class FileExistsEvent extends Event {

	File file;
	
	public FileExistsEvent(File file) {
		super("FileExistsEvent");
		
		this.file = file;
	}

	public File getFile() {
		return file;
	}
}
