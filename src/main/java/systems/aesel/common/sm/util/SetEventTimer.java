package systems.aesel.common.sm.util;

import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import systems.aesel.common.ConfigException;
import systems.aesel.common.sm.IActivity;

public class SetEventTimer extends IActivity {
	
	private static Logger swLogger = Logger.getLogger(SetEventTimer.class);
	private Timer timer;
	private TimerEvent event;
	
	long delay=2500;
	long period = -1;
	boolean hasPeriod = false;
	
	
	public SetEventTimer(TimerEvent event) {
		this.event = event;
	}
	
	
	public Timer getTimer() {
		return timer;
	}

	@Override
	public void init(Properties props) throws ConfigException {
		super.init(props);		
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}
	
	public void setPeriod(long period) {
		this.period = period;
		hasPeriod = true;
	}

	public void run() {
		
		timer = new Timer();
		if(!hasPeriod) {
			timer.schedule(
				new TimerTask() {
	
					@Override
					public void run() {
						try {
							
							machine.eventHappens(event);
							
						} catch (ConfigException e) {
							swLogger.warn(e.getMessage(),e);
						}
					}
				}, 
				delay);
			
		} else {
			timer.schedule(
				new TimerTask() {
	
					@Override
					public void run() {
						try {
							
							machine.eventHappens(event);
							
						} catch (ConfigException e) {
							swLogger.warn(e.getMessage(),e);
						}
					}
				}, 
				delay,
				period);
		}
	}
}
