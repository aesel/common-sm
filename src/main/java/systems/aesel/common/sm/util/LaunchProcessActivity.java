package systems.aesel.common.sm.util;

import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;

import systems.aesel.common.sm.Activity;
import systems.aesel.common.sm.StateMachine;

public class LaunchProcessActivity implements Activity {

	static Logger ConfigLogger = Logger.getLogger("ConfigLogger");
	static Logger IntLogger = Logger.getLogger("IntegrationLogger");
	
	protected final static String FileNameTemplate = "PREFIX-DATE-TIME.EXTEN";
	
	public final static String Prop_PreviousLaunchedProcess = "PreviousLaunchedProcess";
	public final static String Prop_LaunchedProcess = "LaunchedProcess";
	
	protected String commandLine;
	protected String outputFilePrefix;
	protected String extension;
	protected String filename;
	protected long duration; // in seconds
	
	protected StateMachine machine;
	protected Process process;
	
	public LaunchProcessActivity(String commandLine, String outputFilePrefix,
			String extension, long duration) {
		super();
		this.commandLine = commandLine;
		this.outputFilePrefix = outputFilePrefix;
		this.extension = extension;
		this.duration = duration;
	}

	
	public Process getProcess() {
		return process;
	}

	private static String twoDigit(long value) {
		String returnValue = "" + value;
		if(returnValue.length() == 1) {
			returnValue = "0" + returnValue;
		}
		return returnValue;
	}
	
	@Override
	public void run() {
		IntLogger.debug("LaunchProcessActivity.run()");
		
		Runtime rt = Runtime.getRuntime();
		
		Calendar cal = Calendar.getInstance();
		
		String cmdToExec = commandLine.replaceFirst("PREFIX", outputFilePrefix);
		cmdToExec = cmdToExec.replaceFirst("DATE", getDateString(cal));
		cmdToExec = cmdToExec.replaceFirst("TIME", getTimeString(cal));
		cmdToExec = cmdToExec.replaceFirst("EXTEN", extension);
		
		long timeToLive = duration;
		long hours = timeToLive / (60*60); timeToLive -= (hours*(60*60));
		long mins = timeToLive / 60; timeToLive -= (mins * 60);
		long secs = timeToLive;
		
		cmdToExec = cmdToExec.replaceFirst("DURATION", twoDigit(hours) + ":" + twoDigit(mins) + ":" +twoDigit(secs));
		
		String filename = FileNameTemplate.replaceFirst("PREFIX", outputFilePrefix);
		filename = filename.replaceFirst("DATE", getDateString(cal));
		filename = filename.replaceFirst("TIME", getTimeString(cal));
		filename = filename.replaceFirst("EXTEN", extension);
		
		this.filename = filename;
		IntLogger.debug("New Process output filename: " + this.filename);
		
		IntLogger.debug("executing " + cmdToExec);
		try {
			Object previousObj = machine.getFromMachineState(Prop_LaunchedProcess);
			if(previousObj != null) {
				machine.putFromMachineState(Prop_PreviousLaunchedProcess, (Process) previousObj);
			}
			
			process = rt.exec(cmdToExec);
			
			machine.putFromMachineState(Prop_LaunchedProcess, process);
			
		} catch (IOException e) {
			
			IntLogger.warn("Process launch threw an exception. Review command for dependencies and gaps.", e);
			e.printStackTrace();
		}
		
	}
	
	public String getFilename() {
		return filename;
	}

	protected String getDateString(Calendar cal) { 
		return "" + 
				cal.get(Calendar.YEAR) + 
				forceLength(cal.get(Calendar.MONTH)+1,2) +
				forceLength(cal.get(Calendar.DAY_OF_MONTH),2);
	}
	
	protected String getTimeString(Calendar cal) { 
		return "" + 
				forceLength(cal.get(Calendar.HOUR_OF_DAY),2) + 
				forceLength(cal.get(Calendar.MINUTE),2) +
				forceLength(cal.get(Calendar.SECOND),2);
	}
	
	private String forceLength(int value, int size) {
		String valueStr = "" + value;
		
		while(valueStr.length() < size)
			valueStr = "0" + valueStr;
		
		return valueStr;
	}

	@Override
	public String getName() {
		return "Launch ffmpeg";
	}

	@Override
	public void setStateMachine(StateMachine machine) {
		this.machine = machine;
	}

	@Override
	public void init(Properties props) {
		// TODO Auto-generated method stub
		
	}
	
}
