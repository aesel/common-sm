package systems.aesel.common.sm.util;

import org.apache.log4j.Logger;

import systems.aesel.common.sm.IActivity;

public class CancelEventTimer extends IActivity {

	private static Logger swLogger = Logger.getLogger(CancelEventTimer.class);
	
	SetEventTimer timeSetterActivity;
	
	public CancelEventTimer(SetEventTimer timeSetterActivity) {
		super();
		this.timeSetterActivity = timeSetterActivity;
	}

	public void run() {
		swLogger.debug("CancelEventTimer running");
		timeSetterActivity.getTimer().cancel();
		
	}

}
