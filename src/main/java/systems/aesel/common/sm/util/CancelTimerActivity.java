package systems.aesel.common.sm.util;

import java.util.Properties;
import java.util.Timer;

import systems.aesel.common.sm.Activity;
import systems.aesel.common.sm.StateMachine;

public class CancelTimerActivity implements Activity {

	private Timer timer;
	
	public CancelTimerActivity(Timer timer) {
		super();
		this.timer = timer;
	}

	@Override
	public void run() {
		timer.cancel();
	}

	@Override
	public String getName() {
		return "CancelTimerActivity";
	}
	
	@Override
	public void setStateMachine(StateMachine machine) {
		
	}

	@Override
	public void init(Properties props) {
		// TODO Auto-generated method stub
		
	}

}
