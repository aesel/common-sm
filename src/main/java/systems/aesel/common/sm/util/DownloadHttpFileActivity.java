package systems.aesel.common.sm.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Properties;

import org.apache.log4j.Logger;

import systems.aesel.common.sm.Activity;
import systems.aesel.common.sm.StateMachine;


public class DownloadHttpFileActivity implements Activity {

	static Logger ConfigLogger = Logger.getLogger("friz.video.nvr.Config");
	static Logger IntLogger = Logger.getLogger("friz.video.nvr.Integration");
	static Logger swLogger = Logger.getLogger(DownloadHttpFileActivity.class);
	
	private String filename;
	private String urlToDownload;
	
	public DownloadHttpFileActivity(String filename, String urlToDownload) {
		super();
		this.filename = filename;
		this.urlToDownload = urlToDownload;
	}

	@Override
	public void run() {
		try {
			if( urlToDownload == null || urlToDownload.isEmpty() ) {
				ConfigLogger.warn("URL is not provided.");
				return;
			}
			
			if( filename == null || filename.isEmpty() ) {
				ConfigLogger.warn("HTTP image download filename is not provided. (FTPDestinationFileName)");
				return;
			}
			
			URL website = new URL(urlToDownload);
		
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			FileOutputStream fos = new FileOutputStream(filename);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
		
		} catch (MalformedURLException e) {
			ConfigLogger.warn("URL is not correct." , e);
			throw new RuntimeException(e);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
		}
			
	}

	@Override
	public String getName() {
		return "DownloadHttpFileActivity";
	}

	@Override
	public void setStateMachine(StateMachine machine) {
		// no-op

	}

	@Override
	public void init(Properties props) {
		// TODO Auto-generated method stub
		
	}

}
