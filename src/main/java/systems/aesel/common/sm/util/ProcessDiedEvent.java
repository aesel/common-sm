package systems.aesel.common.sm.util;

import systems.aesel.common.sm.Event;

public class ProcessDiedEvent extends Event {

	private int exitCode;
	
	public ProcessDiedEvent(int exitCode) {
		super("ProcessDiedEvent");
		
		this.exitCode = exitCode;
	}

	public int getExitCode() {
		return exitCode;
	}
}
