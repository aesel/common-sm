package systems.aesel.common.sm;

import java.util.Properties;

import systems.aesel.common.ConfigException;

/**
 * An activity is run upon entering a state. They are run serially. The run() function should
 * return in a timely fasion - as it is holding up any other state processing. 
 * 
 * @author 1042090
 *
 */
public interface Activity extends Runnable {
	
	public void init(Properties props) throws ConfigException;
	
	public String getName();
	
	public void setStateMachine(StateMachine machine);
}
