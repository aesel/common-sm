package systems.aesel.common.sm;

import java.util.Properties;

import systems.aesel.common.ConfigException;

public abstract class IActivity implements Activity {

	protected StateMachine machine;
	protected Properties props;
	
	@Override
	public String getName() {
		return this.getClass().getName();
	}

	@Override
	public void setStateMachine(StateMachine machine) {
		this.machine = machine;

	}

	@Override
	public void init(Properties props) throws ConfigException {
		this.props = props;
	}
	
}
