package systems.aesel.common;

public class ConfigException extends Exception {

	public ConfigException(String message) {
		super(message);
	}

}
